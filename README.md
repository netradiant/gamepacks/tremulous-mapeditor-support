# Tremulous map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Tremulous.

This gamepack is based on the game pack provided by Ingar:

- http://ingar.intranifty.net/files/netradiant/gamepacks/TremulousPack.zip

More stuff may be imported from http://svn.icculus.org/gtkradiant-gamepacks/TremulousPack/ in the future.
